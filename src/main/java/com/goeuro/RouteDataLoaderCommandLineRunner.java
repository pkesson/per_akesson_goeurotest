package com.goeuro;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.goeuro.repository.RouteDataRepository;

@Component
public class RouteDataLoaderCommandLineRunner implements CommandLineRunner{
		
		@Autowired
		private RouteDataRepository routeDataRepository;
		
		@Override
		public void run(String... argv) throws Exception {
			if (argv.length > 0){
				String routeDataFileName = argv[0];
				File routeDataFile = new File(routeDataFileName);
				routeDataRepository.loadRouteDataFromFile(routeDataFile);
			}
		}
}

