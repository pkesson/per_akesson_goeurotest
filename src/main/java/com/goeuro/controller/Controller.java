package com.goeuro.controller;

import com.goeuro.model.StationModel;
import com.goeuro.service.RouteService;
import com.goeuro.model.ResponseModel;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping(path = "/rest/provider/goeurobus/direct/{dep_sid}/{arr_sid}")
public class Controller {
	
	private RouteService routeService;
	
	@Autowired
	public Controller(RouteService routeService){
		this.routeService = routeService;
	}
	@RequestMapping( method = RequestMethod.GET)
	public ResponseModel getStationInput(@PathVariable("dep_sid") int dep_sid, @PathVariable("arr_sid") int arr_sid) {
		Boolean routeFound = routeService.routeFound(dep_sid, arr_sid);
		
		return new ResponseModel(dep_sid, arr_sid, routeFound);
	}
}	

