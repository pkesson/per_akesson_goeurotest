# Bus Route Solution

### Solution

Solution for Bus Route Challenge. The solution is using the Spring Boot maven plugin. The application opens a csv file and stores it's data in memory. 

### Input

A micro service implementing a REST-API that serves
`http://localhost:8088/rest/provider/goeurobus/direct/:dep_sid/:arr_sid`. The
parameters `dep_sid` (departure) and `arr_sid` (arrival) are two station ids
(sid) represented by 32 bit integers. A csv datafile is loaded by specifying it's path as and
argument when calling the application. 

### Response

The response is a single JSON Object:

```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "dep_sid": {
      "type": "integer"
    },
    "arr_sid": {
      "type": "integer"
    },
    "direct_bus_route": {
      "type": "boolean"
    }
  },
  "required": [
    "dep_sid",
    "arr_sid",
    "direct_bus_route"
  ]
}
```

### Example

Bus Routes Data File:
```
3
0 0 1 2 3 4
1 3 1 6 5
2 0 6 4
```

Query:
````
http://localhost:8088/rest/provider/goeurobus/direct/3/6
````

Response:
```
{
    "dep_sid": 3,
    "arr_sid": 6,
    "direct_bus_route": true
}
```