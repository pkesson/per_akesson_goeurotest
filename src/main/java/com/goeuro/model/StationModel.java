package com.goeuro.model;

public class StationModel {
	
	private int dep_sid;
	private int arr_sid;
	public StationModel(int dep_sid, int arr_sid){
		this.dep_sid = dep_sid;
		this.arr_sid = arr_sid;
	}
	public void setDepId(int id){
		this.dep_sid = id;
	}
	public void setArrId(int id){
		this.arr_sid = id;
	}
	public int getDepId(){
		return this.dep_sid;
	}
	public int getArrId(){
		return this.arr_sid;
	}
}
