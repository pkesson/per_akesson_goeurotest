package com.goeuro.model;

// import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseModel {
	private final int dep_sid;
	private final int arr_sid;
	private final boolean direct_bus_route;
	
	public ResponseModel(int dep_sid, int arr_sid, boolean direct_bus_route){
		this.dep_sid = dep_sid;
		this.arr_sid = arr_sid;
		this.direct_bus_route = direct_bus_route;
	}
	
	// @JsonProperty("getdep_sid") can be used for better java names
	public int getdep_sid()
	{
		return this.dep_sid;
	}
	public int getarr_sid()
	{
		return this.arr_sid;
	}
	public boolean getdirect_bus_route(){
		return this.direct_bus_route;
	}
}
