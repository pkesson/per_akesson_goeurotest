package com.goeuro.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.goeuro.model.StationModel;
import com.goeuro.repository.RouteDataRepository;

@Service
public class RouteService {
	
	private RouteDataRepository routeouteRepository;	
	
	@Autowired
	public RouteService(RouteDataRepository routeRepository) {
		this.routeouteRepository = routeRepository;
	}
	
	public Boolean routeFound(int dep_sid, int arr_sid) {
		Map<Integer, List<Integer>> routeData = routeouteRepository.getRoutes();
		return isDirectRoute(dep_sid, arr_sid, routeData);
	}

	public Boolean isDirectRoute(Integer fromStation, Integer toStation, Map<Integer, List<Integer>> routeData) {
		for (List<Integer> busRoute : routeData.values()) {
			Boolean depFound = false;

			for (Integer station : busRoute) {
				// Assume order is of importance (bus is not turning around)
				if (!depFound && station.equals(fromStation)) {
					depFound = true;
				} else if (depFound && station.equals(toStation)) {
					return true;
				}
			}
		}
		return false;
	}
}
