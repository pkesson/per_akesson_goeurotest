package com.goeuro.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.goeuro.Application;

@RunWith(SpringJUnit4ClassRunner.class)
// @SpringApplicationConfiguration(classes = Application.class)
// @WebAppConfiguration
@SpringBootTest(classes = Application.class, webEnvironment = 
SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ControllerTests {

		@Autowired
		private WebApplicationContext ctx;

		private MockMvc mockMvc;

		@BeforeClass
		public static void globalSetup() {
			System.setProperty("routedata.file", "src/main/resources/RouteData.csv");
		}
		
		@Before
		public void setUp() {
			this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
		}

		@Test
		public void contentTypeOfBusRouteShouldBoolean() throws Exception {

			this.mockMvc.perform(get("http://localhost:8088/rest/provider/goeurobus/direct/2/15"))
					.andDo(print())
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.dep_sid").value(2))
					.andExpect(jsonPath("$.arr_sid").value(15))
					.andExpect(jsonPath("$.direct_bus_route").isBoolean());
		}
}
