package com.goeuro.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
// A simple implementation to open the data file and store it
// in memory. For larger applications one would probably change
// this to using a database model e.g. neo4j
@Repository
public class RouteDataRepository {
	
	Map<Integer, List<Integer>> routeData = new HashMap<>();
		
	public Map<Integer, List<Integer>> getRoutes() {
			return routeData;
	}
	
	private void addRoute(Integer route, List<Integer> stations) {
			routeData.put(route, stations);
	}
	
	public void loadRouteDataFromFile(File file) throws FileNotFoundException {
		
		try (Scanner inputStream = new Scanner(file)){
			inputStream.next(); // Skip first row. This row can be parsed in order to get the
								// total amount of rows. Not used now but might be useful
			while (inputStream.hasNext()) {
				String data = inputStream.next();
				String[] row = data.split(",");
				List<String> colValues = Arrays.asList(row);
				Integer routeId = Integer.valueOf(colValues.get(0));
				List<Integer> stations = colValues.subList(1, colValues.size()).stream().map(Integer::valueOf).collect(Collectors.toList());
				addRoute(routeId, stations);
			}
		} 
	}
}
