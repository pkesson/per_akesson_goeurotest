package com.goeuro.controller;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.goeuro.repository.RouteDataRepository;
import com.goeuro.service.RouteService;

import static org.mockito.Mockito.*;

// A few basic tests, more could and should be added
@RunWith(MockitoJUnitRunner.class)
public class RouteServiceTests {
	
	@Mock
	RouteDataRepository routeDataRepository;
	
	@InjectMocks
	RouteService routeService = new RouteService(routeDataRepository);
	
	Map<Integer, List<Integer>> getTestRouteData() {
		Map<Integer, List<Integer>> testRouteData = new HashMap<Integer, List<Integer>>();
		int[] routes =  {12, 21, 123, 32124, 15, 51, 567, 754 };
				for (int route : routes) {
					testRouteData.put(route, routeGenerator(route));
				}
				return testRouteData;
	}
	
	private List<Integer> routeGenerator(int routeNumber) {
		String str = Integer.toString(routeNumber);
		String[] strArr = str.split("(?!^)");
		Integer[] stations = new Integer[strArr.length];
		for (int i = 0; i < strArr.length; i++){
			stations[i] = Integer.valueOf(strArr[i]);
		}
		return Arrays.asList(stations);
	}
	
	@Test
	public void testNormalInput() {
		when(routeDataRepository.getRoutes()).thenReturn(getTestRouteData());

		assertEquals(true, routeService.routeFound(1,2));
		assertEquals(true, routeService.routeFound(1,3));
		assertEquals(true, routeService.routeFound(2, 3));
		assertEquals(false, routeService.routeFound(1, 7));
		assertEquals(false, routeService.routeFound(3, 7));
		assertEquals(true, routeService.routeFound(5, 7));
		assertEquals(true, routeService.routeFound(6, 7));
		assertEquals(true, routeService.routeFound(7, 5));
		assertEquals(false, routeService.routeFound(6, 5));
	}
}
