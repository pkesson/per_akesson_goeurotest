package com.goeuro;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class Application {
	
	@Autowired
	Environment env;
	
	public static void main(String[] args) {
		
		final Logger log = Logger.getLogger(Application.class);
		SpringApplication springApplication = new SpringApplication(new Object[] {
			Application.class
		});
		
		if(args.length > 0) {
			Map<String, Object> defaultProperties = new HashMap<String, Object>();
			defaultProperties.put("routedata.file", args[0]);
			springApplication.setDefaultProperties(defaultProperties);
			log.info("Default properties from command line:" + defaultProperties);
		}
		springApplication.run(args);
	}
}

